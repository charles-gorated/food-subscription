class CreateFoodSubscriptionMealPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :food_subscription_meal_plans do |t|
      t.string :name
      t.text :description
      t.float :price

      t.timestamps
    end
  end
end
