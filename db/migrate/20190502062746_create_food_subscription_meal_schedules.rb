class CreateFoodSubscriptionMealSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :food_subscription_meal_schedules do |t|
      t.string :schedule
      t.text :description

      t.timestamps
    end
  end
end
