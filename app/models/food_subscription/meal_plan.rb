module FoodSubscription
  class MealPlan < ApplicationRecord
    validates :name, :description, :price, presence: true

  end
end
