module FoodSubscription
  class MealSchedule < ApplicationRecord
    validates :schedule, :description, presence: true

  end
end
